package jp.ac.meiji.igusso.scop4j;

/**
 * linear制約やquadratic制約における比較方法を表すクラス.
 */
public enum Comparator {
  /** <= (Less or Equal). */
  LE,
  /** >= (Greater or Equal). */
  GE,
  /** == (EQual). */
  EQ
}
