package jp.ac.meiji.igusso.model;

public enum Comparator {
  EQ, LE, LT, GE, GT
}
