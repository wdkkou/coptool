# This is a comment. Comments start with #
SECTION_HORIZON
# All instances start on a Monday
# The horizon length in days:
7

SECTION_SHIFTS
# ShiftID, Length in mins, Shifts which cannot follow this shift | separated
L,480,E
E,480,

SECTION_STAFF
# ID, MaxShifts, MaxTotalMinutes, MinTotalMinutes, MaxConsecutiveShifts, MinConsecutiveShifts, MinConsecutiveDaysOff, MaxWeekends
A,L=7|E=7,2700,1418,5,2,2,1
B,L=0|E=7,2700,1418,5,2,2,1
C,L=7|E=7,2700,1418,5,2,2,1
D,L=7|E=7,2700,1418,5,2,2,1
E,L=7|E=7,2700,1418,5,2,2,1
F,L=7|E=0,2700,1418,5,2,2,1
G,L=7|E=7,2700,1418,5,2,2,1
H,L=7|E=7,2700,1418,5,2,2,1
I,L=7|E=7,2700,1418,5,2,2,1
J,L=7|E=7,2700,1418,5,2,2,1

SECTION_DAYS_OFF
# EmployeeID, DayIndexes (start at zero)
A
B
C
D
E
F
G
H
I
J

SECTION_SHIFT_ON_REQUESTS
# EmployeeID, Day, ShiftID, Weight
C,1,E,3
C,2,E,3
C,6,E,3
E,2,E,3
E,3,E,3
E,4,E,3
E,5,E,3
G,6,L,3
I,0,E,2
I,1,E,2
I,2,E,2

SECTION_SHIFT_OFF_REQUESTS
# EmployeeID, Day, ShiftID, Weight
F,3,L,1
F,4,L,1
F,5,L,1
F,6,L,1
A,5,L,50
A,5,E,50
A,6,L,50
A,6,E,50
D,2,L,50
D,2,E,50

SECTION_COVER
# Day, ShiftID, Requirement, Weight for under, Weight for over
0,E,2,100,1
0,L,3,100,1
1,E,3,100,1
1,L,3,100,1
2,E,4,100,1
2,L,4,100,1
3,E,2,100,1
3,L,3,100,1
4,E,2,100,1
4,L,3,100,1
5,E,2,100,1
5,L,3,100,1
6,E,2,100,1
6,L,3,100,1

