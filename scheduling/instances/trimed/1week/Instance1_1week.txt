# This is a comment. Comments start with #
SECTION_HORIZON
# All instances start on a Monday
# The horizon length in days:
7

SECTION_SHIFTS
# ShiftID, Length in mins, Shifts which cannot follow this shift | separated
D,480,

SECTION_STAFF
# ID, MaxShifts, MaxTotalMinutes, MinTotalMinutes, MaxConsecutiveShifts, MinConsecutiveShifts, MinConsecutiveDaysOff, MaxWeekends
A,D=7,2700,1260,5,2,2,1
B,D=7,2700,1260,5,2,2,1
C,D=7,2700,1260,5,2,2,1
D,D=7,2700,1260,5,2,2,1
E,D=7,2700,1260,5,2,2,1
F,D=7,2700,1260,5,2,2,1
G,D=7,2700,1260,5,2,2,1
H,D=7,2700,1260,5,2,2,1

SECTION_DAYS_OFF
# EmployeeID, DayIndexes (start at zero)
A
B
C
D
E
F
G
H

SECTION_SHIFT_ON_REQUESTS
# EmployeeID, Day, ShiftID, Weight
A,2,D,2
A,3,D,2
B,0,D,3
B,1,D,3
B,2,D,3
B,3,D,3
B,4,D,3
C,0,D,1
C,1,D,1
C,2,D,1
C,3,D,1
C,4,D,1
F,0,D,2
F,1,D,2

SECTION_SHIFT_OFF_REQUESTS
# EmployeeID, Day, ShiftID, Weight
H,2,D,3
H,3,D,3
A,0,D,50
B,5,D,50
D,2,D,50
F,5,D,50
G,1,D,50

SECTION_COVER
# Day, ShiftID, Requirement, Weight for under, Weight for over
0,D,5,100,1
1,D,7,100,1
2,D,6,100,1
3,D,4,100,1
4,D,5,100,1
5,D,5,100,1
6,D,5,100,1

