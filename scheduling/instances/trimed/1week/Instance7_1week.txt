# This is a comment. Comments start with #
SECTION_HORIZON
# All instances start on a Monday
# The horizon length in days:
7

SECTION_SHIFTS
# ShiftID, Length in mins, Shifts which cannot follow this shift | separated
D,480,E
L,480,E|D
E,480,

SECTION_STAFF
# ID, MaxShifts, MaxTotalMinutes, MinTotalMinutes, MaxConsecutiveShifts, MinConsecutiveShifts, MinConsecutiveDaysOff, MaxWeekends
A,D=7|L=0|E=7,2700,1418,5,2,2,1
B,D=7|L=0|E=7,2700,1418,5,2,2,1
C,D=7|L=2|E=7,2700,1418,5,2,2,1
D,D=0|L=0|E=7,2700,1418,5,2,2,1
E,D=7|L=2|E=7,2700,1418,5,2,2,1
F,D=0|L=2|E=7,2700,1418,5,2,2,1
G,D=0|L=0|E=7,2700,1418,5,2,2,1
H,D=7|L=2|E=7,2700,1418,5,2,2,1
I,D=7|L=0|E=7,2700,1418,5,2,2,1
J,D=7|L=2|E=7,2700,1418,5,2,2,1
K,D=7|L=2|E=7,2700,1418,6,2,3,1
L,D=7|L=2|E=7,2700,1418,6,2,3,1
M,D=7|L=2|E=7,2700,1418,6,2,3,1
N,D=7|L=2|E=7,2700,1418,6,2,3,1
O,D=7|L=0|E=7,2700,1418,6,2,3,1
P,D=7|L=1|E=0,1350,608,5,1,2,1
Q,D=7|L=0|E=7,1350,608,5,1,2,1
R,D=7|L=1|E=7,1350,608,5,1,2,1
S,D=7|L=1|E=7,1350,608,5,1,2,1
T,D=7|L=0|E=7,1350,608,5,1,2,1

SECTION_DAYS_OFF
# EmployeeID, DayIndexes (start at zero)
A
B
C
D
E
F
G
H
I
J
K
L
M
N
O
P
Q
R
S
T

SECTION_SHIFT_ON_REQUESTS
# EmployeeID, Day, ShiftID, Weight
A,2,D,2
A,3,D,2
E,0,L,1
E,1,L,1
E,2,L,1
E,3,L,1
F,0,L,3
F,1,L,3
K,0,E,2
M,0,E,3
N,1,L,3
N,2,L,3
N,3,L,3
N,4,L,3
N,5,L,3
O,2,D,2
O,3,D,2
O,4,D,2
O,5,D,2
P,2,L,3
P,3,L,3
P,4,L,3
P,5,L,3
P,6,L,3
Q,1,D,2
Q,2,D,2
Q,3,D,2
Q,4,D,2
Q,5,D,2
R,1,L,2
R,2,L,2
R,3,L,2
R,4,L,2
S,1,D,1
S,2,D,1

SECTION_SHIFT_OFF_REQUESTS
# EmployeeID, Day, ShiftID, Weight
B,2,D,2
B,6,E,1
D,2,E,2
D,3,E,2
D,4,E,2
G,2,E,2
G,3,E,2
H,1,D,2
H,2,D,2
I,3,E,1
I,4,E,1
I,5,E,1
J,0,E,1
J,1,E,1
J,2,E,1
J,3,E,1
J,4,E,1
K,4,L,3
C,1,D,50
C,1,L,50
C,1,E,50
C,6,D,50
C,6,L,50
C,6,E,50
M,3,D,50
M,3,L,50
M,3,E,50
M,4,D,50
M,4,L,50
M,4,E,50
S,3,D,50
S,3,L,50
S,3,E,50

SECTION_COVER
# Day, ShiftID, Requirement, Weight for under, Weight for over
0,E,4,100,1
0,D,6,100,1
0,L,3,100,1
1,E,4,100,1
1,D,6,100,1
1,L,1,100,1
2,E,4,100,1
2,D,6,100,1
2,L,2,100,1
3,E,4,100,1
3,D,5,100,1
3,L,1,100,1
4,E,3,100,1
4,D,7,100,1
4,L,2,100,1
5,E,4,100,1
5,D,4,100,1
5,L,3,100,1
6,E,4,100,1
6,D,7,100,1
6,L,2,100,1

